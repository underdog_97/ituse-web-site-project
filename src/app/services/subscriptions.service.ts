import { Injectable } from '@angular/core';
import { Subscription } from '../models/Subscription';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { DatePipe } from '@angular/common';
import { UUID } from 'angular2-uuid';
import { TgBotService } from './tg-bot.service';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionsService {

  myDate: any;
  subDoc: AngularFirestoreDocument<Subscription>;
  constructor(public afs: AngularFirestore, private datePipe: DatePipe, private bot: TgBotService) { }

  currentDate() {
    this.myDate = new Date()
  }

  generateId() {
    return UUID.UUID();
  }

  addSubscription(subscription: Subscription) {
    this.currentDate();
    this.afs.collection("subscriptions").doc(subscription.email)
    .set({
      email: subscription.email,
      date: this.myDate,
      id: this.generateId()
    })
    const body = {
      email: subscription.email
    }
    this.bot.postSubscription(body).subscribe();
  }
  deleteSubscription(subscription: Subscription) {
    this.afs.collection('subscriptions').doc(subscription.email).delete().then(() => {
    })
  }
}
