import { TestBed } from '@angular/core/testing';

import { TeamConfigService } from './team-config.service';

describe('TeamConfigService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TeamConfigService = TestBed.get(TeamConfigService);
    expect(service).toBeTruthy();
  });
});
