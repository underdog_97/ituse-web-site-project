import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { AngularFireStorage } from 'angularfire2/storage';
import { UUID } from 'angular2-uuid';
import { Portfolio_Cat } from '../models/Portfolio_Cat';
import { Observable } from 'rxjs';
import { Image } from '../models/Image';


@Injectable({
  providedIn: 'root'
})
export class PortfolioService {

  constructor(private afs: AngularFirestore,
    private afStorage: AngularFireStorage) { }

  categoriesCol: AngularFirestoreCollection<Portfolio_Cat>;
  categories: Observable<Portfolio_Cat[]>;

  getCategories() {
    this.categoriesCol = this.afs.collection('portfolioCats');
    this.categories = this.categoriesCol.valueChanges();
    return this.categories;
  }
  
  addCategory(portfolioCat: Portfolio_Cat) {
    let id = UUID.UUID();
    this.categoriesCol.doc(id).set({
      images: portfolioCat.images,
      position: portfolioCat.position,
      name: portfolioCat.name,
      id: id
    })
  }

  deleteCategory(portfolioCat: Portfolio_Cat) {
    this.afs.collection('portfolioCats').doc(portfolioCat.id).delete().then(() => {
    })
  }

  renameCat(portfolioCat: Portfolio_Cat) {
    this.afs.collection("portfolioCats").doc(portfolioCat.id)
    .set({
      images: portfolioCat.images,
      name: portfolioCat.name,
      position: portfolioCat.position,
      id: portfolioCat.id
    })
  }

}
