import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Worker } from '../models/Worker';
import { UUID } from 'angular2-uuid';
import { Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class TeamConfigService {

  teamCol: AngularFirestoreCollection<Worker>;
  workers: Observable<Worker[]>;

  constructor(public afs: AngularFirestore,
    private _snackBar: MatSnackBar) { }

  addWorker(worker: Worker) {
    var id = this.generateId();
    this.afs.collection("team").doc(id)
    .set({
      name: worker.name,
      description: worker.description,
      socials: worker.socials,
      photo_url: worker.photo_url,
      id: id,
      position: worker.position
    })
  }

  deleteWorker(worker: Worker) {
    this.afs.collection('team').doc(worker.id).delete().then(() => {
      this._snackBar.open(worker.name + " deleted from our team!", 'OK', {
        duration: 3000,
      });
    });
  }

  generateId() {
    return UUID.UUID();
  }

  saveTeamList(teamList) {
    for (let i = 0; i < teamList.length; i++) {
      this.afs.collection("team").doc(teamList[i].id).set({
        name: teamList[i].name,
        description: teamList[i].description,
        socials: teamList[i].socials,
        photo_url: teamList[i].photo_url,
        id: teamList[i].id,
        position: teamList[i].position
      })
    }
  }

  saveTeammate(mate) {
    this.afs.collection("team").doc(mate.id).set({
      name: mate.name,
      description: mate.description,
      socials: mate.socials,
      photo_url: mate.photo_url,
      id: mate.id,
      position: mate.position
    })
  }

}

