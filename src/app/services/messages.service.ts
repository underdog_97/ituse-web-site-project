import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { UUID } from 'angular2-uuid';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Message } from '../models/Message';
import { TgBotService } from './tg-bot.service';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {
  myDateSimple: any;
  myDate24Hours: any;
  constructor(public afs: AngularFirestore, 
    private readonly datePipe: DatePipe,
    public bot: TgBotService) {

   }

  currentDate() {
    this.myDate24Hours = this.datePipe.transform(new Date(), 'EEEE, dd/MM/yyyy, HH:mm');
    this.myDateSimple = this.datePipe.transform(new Date(), 'dd/MM ~ HH:mm');
  }

  generateId() {
    return UUID.UUID();
  }

  addMessage(message: Message) {
    let id = this.generateId();
    this.currentDate();
    this.afs.collection('messages').doc(message.email + '. ID: ' + id)
    .set({
      name: message.name,
      email: message.email,
      message: message.message,
      date: new Date(),
      id,
      read: false,
      ip: message.ip,
      city: message.city,
      region: message.region,
      country: message.country,
      favorite: false,
      trash: false,
      trashDate: null
    })
    .catch(error => {
      console.error('Error:', error);
    });
    const body = {
      name: message.name,
      email: message.email
    }
    this.bot.postMessage(body).subscribe();
  }

  deleteMessage(message: Message) {
    this.afs.collection('messages')
      .doc(message.email + '. ID: ' + message.id)
      .delete();
   }
}
