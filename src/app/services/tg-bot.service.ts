import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TgBotService {

  clientIp = '';
  ip = '134.209.230.20';
  port = '3000';

  urlMessage = `http://${this.ip}:${this.port}/post/message`;
  urlSubscription = `http://${this.ip}:${this.port}/post/subscription`;
  urlLogin = `http://${this.ip}:${this.port}/post/login`;
  urlFailLogin = `http://${this.ip}:${this.port}/post/failLogin`;

  constructor(private http: HttpClient) {
    this.checkIp()
  }

  postMessage(body) {
    return this.http.post<any>(this.urlMessage, body);
  }

  postSubscription(body) {
    return this.http.post<any>(this.urlSubscription, body);
  }

  postLogin() {

  if (this.clientIp.length < 1) {
    this.checkIp()
  }

    let body = {
      ip: this.clientIp
    }

    return this.http.post<any>(this.urlLogin, body);
  }

  postFailLogin() {
    
    if (this.clientIp.length < 1) {
      this.checkIp()
    }

    let body = {
      ip: this.clientIp
    }
    return this.http.post<any>(this.urlFailLogin, body);
  }

  checkIp() {
    try {
      this.http.get('https://api.ipify.org/?format=json')
      .subscribe(data => {
        this.clientIp = data['ip'];
    });
    }
    catch (e) {}
  }

}
