import { TestBed } from '@angular/core/testing';

import { TgBotService } from './tg-bot.service';

describe('TgBotService', () => {
  let service: TgBotService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TgBotService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
