import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { UUID } from 'angular2-uuid';
import { AngularFireStorage } from 'angularfire2/storage';
import { Portfolio_Cat } from '../models/Portfolio_Cat';
import { Observable } from 'rxjs';
import { Image } from '../models/Image';
import { finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  storageCol: AngularFirestoreCollection<Image>;
  images: Observable<Image[]>;

  getItems() {
    this.storageCol = this.afs.collection('storage');
    this.images = this.storageCol.valueChanges();
    return this.images;
  }

  constructor(private afStorage: AngularFireStorage,
    private afs: AngularFirestore) { }

  generateId() {
    return UUID.UUID();
  }

  addImage(event, fileName, path) {
    let downloadURL;
    const file = event.target.files[0];
    const filePath = path + '/' + fileName;
    const fileRef = this.afStorage.ref(filePath);
    const task = this.afStorage.upload(filePath, file);
    task.snapshotChanges().pipe(
      finalize(() => {
        fileRef.getDownloadURL().subscribe(
          response => 
            downloadURL = response,
          error => console.log(error),
          () => {
            let id = this.generateId();
            this.afs.collection("storage").doc(id).set({
            name: fileName,
            id: id,
            folder: path,
            filePath: filePath,
            url: downloadURL,
            cats: []
            })
          })
        })).subscribe()
   }
   deleteImage(item) {
    var filePath, fileRef;
    filePath = item.folder + '/' + item.name;
    fileRef = this.afStorage.ref(filePath);
    fileRef.delete();
    this.deleteInfo(item);
   } 
   deleteInfo(item) {
    this.afs.collection('storage').doc(item.id).delete()
   }
}
