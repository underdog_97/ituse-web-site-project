import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(public authService: AuthService, public router: Router) { }
  
  canActivate(): boolean {
    if (this.authService.isLogin() != 'true') {
      this.router.navigate(['']);
      return false;
    }
    else {
      return true;
    }
  }
}
