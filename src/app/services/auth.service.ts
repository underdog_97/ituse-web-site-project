import { Injectable, NgZone } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore} from '@angular/fire/firestore';
import { MatDialog } from '@angular/material'
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { TgBotService } from './tg-bot.service';
  
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  userData: any;

  constructor(public afs: AngularFirestore,   // Inject Firestore service
              public afAuth: AngularFireAuth, // Inject Firebase auth service
              public router: Router,
              public ngZone: NgZone,
              public dialog: MatDialog,
              private readonly _snackBar: MatSnackBar,
              private bot: TgBotService) {



      this.afAuth.authState.subscribe(user => {
        if (user) {
          this.userData = user;
          localStorage.setItem('user', JSON.stringify(this.userData));
          JSON.parse(localStorage.getItem('user'));
        } else {
          localStorage.setItem('user', null);
          JSON.parse(localStorage.getItem('user'));
        }
      });
    }

  SignIn(email, password) {
      return this.afAuth.auth.signInWithEmailAndPassword(email, password).then(() => {
        this.sendSnackBar('Welcome!', 'accept', 3000);
        this.bot.postLogin().subscribe();
      })

        .then(() => {
          this.ngZone.run(() => {
            localStorage.setItem('isLogin', 'true');
            this.router.navigate(['/messages']);
          });
        })
        .catch(error => {
          // Handle Errors here.
          let errorCode = error.code;
          let errorMessage = error.message;
          this.sendSnackBar('Could not find a user with this email or password incorrect', 'warning', 3000);
          this.bot.postFailLogin().subscribe();
        });

    }

  SignOut() {
      return this.afAuth.auth.signOut().then(() => {
        this.sendSnackBar('Good luck!', 'accept', 3000);
        localStorage.removeItem('user');
        this.router.navigate(['']);
        localStorage.setItem('isLogin', 'false');
      });
    }

  closeDialog() {
      this.dialog.closeAll();
    }
  isLogin() {
      return localStorage.getItem('isLogin');
    }
  checkStatus() {
      if (this.afAuth.auth.currentUser) {
        return true;
      } else {
        return false;
      }
     }
  sendSnackBar(message, type, time) {
      if (type == 'warning') {
        this._snackBar.open(message, 'OK', {
          duration: time,
          verticalPosition: 'bottom',
          horizontalPosition: 'center',
          panelClass: ['warning']
        });
      } else if (type == 'accept') {
        this._snackBar.open(message, 'OK', {
          duration: time,
          verticalPosition: 'bottom',
          horizontalPosition: 'center',
          panelClass: ['accept-snackbar']
        });
      }
    }
}
