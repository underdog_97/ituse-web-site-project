import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { MenuComponent } from './components/menu/menu.component';
import { ProvideComponent } from './components/provide/provide.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FooterComponent } from './components/footer/footer.component';
import { IntouchComponent } from './components/intouch/intouch.component';
import { LoginComponent } from './components/login/login.component';
import { ProductsComponent } from './components/products/products.component';
import { SubscribeComponent } from './components/subscribe/subscribe.component';
import { TeamComponent } from './components/team/team.component';
import { MaterialModule } from './material/material.module';

import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreModule } from 'angularfire2/firestore';
import { environment } from '../environments/environment';

import { DatePipe } from '@angular/common';
import { StorageServiceModule } from 'ngx-webstorage-service';
import { MainComponent } from './components/main/main.component';
import { MessagesComponent } from './components/messages/messages.component';
import { PortfolioConfigComponent } from './components/portfolio-config/portfolio-config.component';
import { ZoomImageComponent } from './components/products/zoom-image/zoom-image.component';
import { DialogPopupComponent } from './components/storage/dialog-popup/dialog-popup.component';
import { StorageComponent } from './components/storage/storage.component';
import { DialogComponent } from './components/subscriptions/dialog/dialog.component';
import { SubscriptionsComponent } from './components/subscriptions/subscriptions.component';
import { SelectImageDialogComponent } from './components/team-config/dialog/select-image-dialog/select-image-dialog.component';
import { TeamConfigDialogComponent } from './components/team-config/dialog/team-config-dialog/team-config-dialog.component';
import { TeamConfigComponent } from './components/team-config/team-config.component';
import { FavoriteFilterPipe } from './pipes/favorite-filter.pipe';
import { FilterByDatePipe } from './pipes/filter-by-date.pipe';
import { SortGalleryPipe } from './pipes/sort-gallery.pipe';
import { SortReadUnreadPipe } from './pipes/sort-read-unread.pipe';
import { TeammatesSortPipe } from './pipes/teammates-sort.pipe';
import { AuthService } from './services/auth.service';
import { SpinnerComponent } from './components/spinner/spinner.component';
@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    HeaderComponent,
    ProvideComponent,
    ProductsComponent,
    TeamComponent,
    SubscribeComponent,
    IntouchComponent,
    FooterComponent,
    LoginComponent,
    MessagesComponent,
    SubscriptionsComponent,
    MainComponent,
    DialogComponent,
    FavoriteFilterPipe,
    FilterByDatePipe,
    SortReadUnreadPipe,
    TeamConfigComponent,
    TeamConfigDialogComponent,
    TeammatesSortPipe,
    PortfolioConfigComponent,
    StorageComponent,
    SortGalleryPipe,
    DialogPopupComponent,
    SelectImageDialogComponent,
    ZoomImageComponent,
    SpinnerComponent
  ],
  imports: [
    AngularFireModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    AngularFireModule.initializeApp(environment.firebase),
    StorageServiceModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [AuthService, DatePipe],
  bootstrap: [AppComponent],
  entryComponents: [
    LoginComponent,
    DialogComponent,
    TeamConfigDialogComponent,
    DialogPopupComponent,
    SelectImageDialogComponent,
    ZoomImageComponent
  ]
})
export class AppModule { }
