import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { MainComponent } from './components/main/main.component';
import { MessagesComponent } from './components/messages/messages.component';
import { PortfolioConfigComponent } from './components/portfolio-config/portfolio-config.component';
import { StorageComponent } from './components/storage/storage.component';
import { SubscriptionsComponent } from './components/subscriptions/subscriptions.component';
import { TeamConfigComponent } from './components/team-config/team-config.component';
import { AuthGuardService } from './services/auth-guard.service';
const routes: Routes = [
  { path: '', component: MainComponent},
  { path: 'portfolioConfig', component: PortfolioConfigComponent, canActivate: [AuthGuardService]},
  { path: 'storage', component: StorageComponent, canActivate: [AuthGuardService] },
  { path: 'teamConfig', component: TeamConfigComponent, canActivate: [AuthGuardService]},
  { path: 'messages', component: MessagesComponent, canActivate: [AuthGuardService] },
  { path: 'subscriptions', component: SubscriptionsComponent, canActivate: [AuthGuardService] },
  { path: 'admin-login', component: LoginComponent}
];
@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: false})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
