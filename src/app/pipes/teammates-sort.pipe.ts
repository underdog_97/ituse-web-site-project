import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'teammatesSort'
})
export class TeammatesSortPipe implements PipeTransform {

  transform(value: any[], currentIndex: number): any {
    let array = [];
    if (value.length == 0 || value.length == 1 || value.length == 2) {
      return value
    }
    else {
      if (currentIndex == 0) {
        array.push(value[currentIndex]);
        array.push(value[currentIndex+1]);
        array.push(value[currentIndex+2]);
        return array;
    } 
    else if (currentIndex >= 1 && currentIndex != value.length - 1) {
      array.push(value[currentIndex-1]);
      array.push(value[currentIndex]);
      array.push(value[currentIndex+1]);
      return array;
    }
    else if (currentIndex == value.length - 1) {
      array.push(value[currentIndex-2]);
      array.push(value[currentIndex-1]);
      array.push(value[currentIndex]);
      return array;
    }
  }
}
}