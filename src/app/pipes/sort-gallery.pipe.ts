import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortGallery'
})
export class SortGalleryPipe implements PipeTransform {

  transform(value: any, sortBy: string): any {
    if (sortBy == 'all') {
      return value;
    }
    return value.filter(value => value.folder == sortBy);
  }

}
