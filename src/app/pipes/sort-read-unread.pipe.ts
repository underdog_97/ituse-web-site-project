import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortReadUnread'
})
export class SortReadUnreadPipe implements PipeTransform {

  transform(value: any, sortReadUnread: string, listOfMessages: any): any {
    if (sortReadUnread == 'default') {
      return listOfMessages;
    }
    else if (sortReadUnread == 'unread') {
      console.log('unread pipe')
      return value.sort((a, b) => {
        return (a.read === b.read) ? 0 : a? -1 : 1;
      });
    }
    else {
      console.log('read pipe')
      return value.sort((a, b) => {
        return (a.read === b.read) ? 0 : a? 1 : -1;
      });
    }
}
}