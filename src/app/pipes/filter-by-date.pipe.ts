import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterByDate'
})
export class FilterByDatePipe implements PipeTransform {

  transform(value: any, dateType: number): any {
    if (dateType == 0) {
      return value;
    }
    else if(dateType == 1) {
      return value.filter(value => <any>new Date().getTime() - <any>new Date(value.newDate).getTime() < 86400000);
    }
    else if(dateType == 2) {
      return value.filter(value => <any>new Date().getTime() - <any>new Date(value.newDate).getTime() < 604800000);
    }
    else if(dateType == 3) {
      return value.filter(value => <any>new Date().getTime() - <any>new Date(value.newDate).getTime() < 2592000000);
    }
    else if(dateType == 4) {
      return value.filter(value => <any>new Date().getTime() - <any>new Date(value.newDate).getTime() < 31536000000);
    }
  }

}
