import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'favoriteFilter'
})
export class FavoriteFilterPipe implements PipeTransform {

  array = [];

  transform(value: Array<any>, filteredBy: string): any {
    if (filteredBy === 'favorite') {
      return value.filter(item => item.favorite === true && item.trash === false);
    } else if (filteredBy === 'trash') {
      return value.filter(item => item.trash === true);
    } else {
      return value.filter(item => item.trash === false);
    }
  }
}