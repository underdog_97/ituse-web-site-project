export interface Subscription {
    email?: string,
    date?: any,
    id?: string,
    ip?: string,
    city?: string,
    region?: string,
    country?: string
}