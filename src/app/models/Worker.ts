export interface Worker {
    name?: string,
    description?: string,
    socials?: Array<Social>,
    photo_url?: string,
    id: string,
    position?: number
}

interface Social{
    social_name?: string,
    profile_link?: string
}