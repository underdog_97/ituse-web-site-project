export interface Image {
    id: string,
    name: string,
    folder: string,
    url: string,
    ref: string,
    category: Array<string>
}