
export interface Message {
    id?: string;
    name?: string;
    email?: string;
    message?: string;
    read?: boolean;
    date?: firebase.firestore.Timestamp;
    ip?: string;
    city?: string;
    region?: string;
    country?: string;
    favorite?: boolean;
    newDate?: Date;
    trash?: boolean;
    trashDate?: Date;
}
