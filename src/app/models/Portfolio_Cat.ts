import { Image } from './../models/Image';
export interface Portfolio_Cat {
    images: Array<string>
    name: string,
    position: number,
    id: string
}