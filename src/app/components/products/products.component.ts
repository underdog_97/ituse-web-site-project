import { Component, OnInit } from '@angular/core';
import { PortfolioService } from 'src/app/services/portfolio.service';
import { Portfolio_Cat } from '../../models/Portfolio_Cat';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogPopupComponent } from '../storage/dialog-popup/dialog-popup.component';
import { ZoomImageComponent } from '../products/zoom-image/zoom-image.component';
@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  portfolioCats: Array<Portfolio_Cat>;
  currentCat: Portfolio_Cat;
  constructor(public ps: PortfolioService,
    public dialog: MatDialog,) {
    
   }

  ngOnInit() {
    this.ps.getCategories().subscribe(value => {
      this.portfolioCats = value.sort((a,b) => {
        return a.position - b.position;
      })
      this.currentCat = this.portfolioCats[0];
    })
  }

  changeCat(cat) {
    this.currentCat = cat;
  }
  getImageUrl(item) {
    return "url('" + item + "')";
  }
  openDialog(item) {
    const dialogRef = this.dialog.open(ZoomImageComponent, {
      minWidth: '10px',
      data: {item: item,
        deleteFunction: false}
    });
  }

  goToLink(url: string){
    window.open(url, "_blank");
  }
}
