import { Component, OnInit } from '@angular/core';
import { SubscriptionsService } from '../../services/subscriptions.service';
import { Subscription } from '../../models/Subscription';
import { FormsModule, FormGroup, FormBuilder } from '@angular/forms';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogComponent } from './dialog/dialog.component';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-subscriptions',
  templateUrl: './subscriptions.component.html',
  styleUrls: ['./subscriptions.component.css']
})
export class SubscriptionsComponent implements OnInit {

  subsCol: AngularFirestoreCollection<Subscription>;
  subs: Observable<Subscription[]>;
  listOfSubs: any;
  dialogRef: any;
  constructor(public dialog: MatDialog, 
    private subService: SubscriptionsService, 
    private afs: AngularFirestore,
    private titleService: Title) { }

  ngOnInit() {
    this.titleService.setTitle("Subscribtions");
    this.subsCol = this.afs.collection('subscriptions');
    this.subs = this.subsCol.valueChanges();
    this.subs.subscribe(value => { 
      for (let i = 0; i < value.length; i++) {
        value[i].date = value[i].date.toDate();
      } 
      this.listOfSubs = value; 
      this.listOfSubs.sort((a, b) =>
        <any>new Date(b.date).getTime() - <any>new Date(a.date).getTime())
    })
  }
  openPopup(sub) {
    this.dialogRef = this.dialog.open(DialogComponent, {
      height: '180px',
      width: '370px',
      data: sub
    });
  }
}
