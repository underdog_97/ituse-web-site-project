import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Subscription } from '../../../models/Subscription';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { SubscriptionsService } from '../../../services/subscriptions.service';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {

  email: string = '';

  constructor(private subService: SubscriptionsService,
     public dialog: MatDialog, 
     @Inject(MAT_DIALOG_DATA) public data: Subscription) {
      }

  ngOnInit() {
  }
  closeDialog(){
    this.dialog.closeAll();
  }
  deleteItem(data) {
    this.subService.deleteSubscription(data);
    this.dialog.closeAll();
  }
}
