import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Message } from '../../models/Message';
import { MessagesService } from '../../services/messages.service';
import 'rxjs/add/operator/map';
import { TgBotService } from 'src/app/services/tg-bot.service';

@Component({
  selector: 'app-intouch',
  styleUrls: ['./intouch.component.css'],
  templateUrl: './intouch.component.html'
})
export class IntouchComponent implements OnInit {
  message: Message = {
    city: '',
    country: '',
    email: '',
    ip: '',
    message: '',
    name: '',
    region: ''
};

  ip = '';

  myForm: FormGroup;
  constructor(private readonly fb: FormBuilder,
              private readonly messagesService: MessagesService,
              private readonly http: HttpClient,
              private readonly _snackBar: MatSnackBar,
              private bot: TgBotService) {

  }

  ngOnInit() {

    this.myForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      message: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(9999)]],
      name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(60)]]
    });

    // this.myForm.valueChanges.subscribe(
    //   console.log
    // );
  }
 sendMessage() {
  if (this.myForm.valid) {
    this.sendSnackBar(this.myForm.value.name + ', your message sended! Wait for your reply on ' + this.myForm.value.email, 'accept', 3000);
    this.message.name = this.myForm.value.name;
    this.message.message = this.myForm.value.message;
    this.message.email = this.myForm.value.email;
    this.message.ip = this.bot.clientIp;
    this.myForm.setValue({
        name: '',
        email: '',
        message: ''
      });
    this.messagesService.addMessage(this.message);
  } else {
      if (this.myForm.value.email == '' || this.myForm.value.name == '' || this.myForm.value.message == '') {
        this.sendSnackBar('All fields must be filled', 'warning', 3000);
      } else if (this.myForm.controls.email.status == 'INVALID') {
        this.sendSnackBar(this.myForm.value.email + ' - invalid email!', 'warning', 3000);
      } else if (this.myForm.value.message.length < 3) {
        this.sendSnackBar('Minimum length of message is 3', 'warning', 3000);
      } else if (this.myForm.value.message.length > 9999) {
        this.sendSnackBar('Maximum length of message is 9999', 'warning', 3000);
      } else if (this.myForm.value.name.length < 3) {
        this.sendSnackBar('Minimum length of name is 3', 'warning', 3000);
      } else if (this.myForm.value.name.length > 60) {
        this.sendSnackBar('Maximum length of name is 60', 'warning', 3000);
      } else {
        this.sendSnackBar('Invalid form', 'warning', 3000);
      }
    }
  }
  sendSnackBar(message, type, time) {
    if (type == 'warning') {
      this._snackBar.open(message, 'OK', {
        duration: time,
        verticalPosition: 'bottom',
        horizontalPosition: 'center',
        panelClass: ['warning']
      });
    } else if (type == 'accept') {
      this._snackBar.open(message, 'OK', {
        duration: time,
        verticalPosition: 'bottom',
        horizontalPosition: 'center',
        panelClass: ['accept-snackbar']
      });
    }
  }
}
