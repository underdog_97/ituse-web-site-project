import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntouchComponent } from './intouch.component';

describe('IntouchComponent', () => {
  let component: IntouchComponent;
  let fixture: ComponentFixture<IntouchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntouchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntouchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
