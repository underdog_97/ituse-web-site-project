import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { Message } from '../../models/Message';
import { MessagesService } from '../../services/messages.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  messageCol: AngularFirestoreCollection<Message>;
  messages: Observable<Array<Message>>;

  unsortedMessages: any;
  listOfMessages: any;
  favorFilter = {favorite: true};
  filteredBy = 'all';
  filterByLast = 0; // 0 default, 1 - last 24, 2 - last week, 3 - last month, 4 - last year
  sortByReadUnreadString = 'default'; // 0 - default, 1 - read, 2 - unread;
  filterByLastString = 'All';
  unreadMessages = 0;
  constructor(private readonly messageService: MessagesService,
              private readonly afs: AngularFirestore,
              private readonly titleService: Title) {

   }

  ngOnInit() {
    this.titleService.setTitle('Messages');
    this.messageCol = this.afs.collection('messages');
    this.messages = this.messageCol.valueChanges();
    this.messages.subscribe(value => {
        for (let i = 0; i < value.length; i++) {
          value[i].newDate = value[i].date.toDate();
          if (value[i].trash) {
            const now = new Date();
            const diffenceInDays = this.diffDates(value[i].newDate, now);
            if (diffenceInDays < -1) {
              this.messageService.deleteMessage(value[i]);
            }
          }
        }
        this.listOfMessages = value;
        this.listOfMessages.sort((a, b) =>
         new Date(b.newDate).getTime() as any -  new Date(a.newDate).getTime() as any);
        this.unsortedMessages = this.listOfMessages;
    });
  }

  diffDates(dayOne, dayTwo) {
    return (dayOne - dayTwo) / (60 * 60 * 24 * 1000);
  }

  filterDateToggle() {
    if (this.filterByLast == 4) {
      this.filterByLast = 0;
      this.filterByLastString = 'all';
    } else if (this.filterByLast == 0) {
      this.filterByLast = 1;
      this.filterByLastString = 'last 24 hours';
    } else if (this.filterByLast == 1) {
      this.filterByLast = 2;
      this.filterByLastString = 'last week';
    } else if (this.filterByLast == 2) {
      this.filterByLast = 3;
      this.filterByLastString = 'last month';
    } else if (this.filterByLast == 3) {
      this.filterByLast = 4;
      this.filterByLastString = 'last year';
    }
  }
  changeFilter(folder) {
    this.filteredBy = folder;
  }
  sortReadUnreadToggle() {
    if (this.sortByReadUnreadString == 'default') {
      this.sortByReadUnreadString = 'unread';
    } else if (this.sortByReadUnreadString == 'unread') {
      this.sortByReadUnreadString = 'read';
    } else if (this.sortByReadUnreadString == 'read') {
      this.sortByReadUnreadString = 'default';
    }
  }

  openCloseToggle(message) {
    console.log('openCloseToggle');
    if (message.read == false) {
      this.messageCol.doc(message.email + '. ID: ' + message.id).set({
        city: message.city,
        country: message.country,
        date: message.date,
        email: message.email,
        id: message.id,
        ip: message.ip,
        message: message.message,
        name: message.name,
        region: message.region,
        read: true,
        favorite: message.favorite,
        trash: message.trash,
        trashDate: message.trashDate
      });
    } else {
      this.messageCol.doc(message.email + '. ID: ' + message.id).set({
        city: message.city,
        country: message.country,
        date: message.date,
        email: message.email,
        id: message.id,
        ip: message.ip,
        message: message.message,
        name: message.name,
        region: message.region,
        read: false,
        favorite: message.favorite,
        trash: message.trash,
        trashDate: message.trashDate
      });
    }
  }
  markAllAsRead() {
    for (let i = 0; i < this.listOfMessages.length; i ++) {
      this.messageCol.doc(this.listOfMessages[i].email + '. ID: ' + this.listOfMessages[i].id).set({
        city: this.listOfMessages[i].city,
        country: this.listOfMessages[i].country,
        date: this.listOfMessages[i].date,
        email: this.listOfMessages[i].email,
        id: this.listOfMessages[i].id,
        ip: this.listOfMessages[i].ip,
        message: this.listOfMessages[i].message,
        name: this.listOfMessages[i].name,
        region: this.listOfMessages[i].region,
        read: true,
        favorite: this.listOfMessages[i].favorite,
        trashDate: this.listOfMessages[i].trashDate,
        trash: this.listOfMessages[i].trash
      });
    }
  }
  favoriteUnfavorite(message) {
    if (message.favorite == false) {
      this.messageCol.doc(message.email + '. ID: ' + message.id).set({
        city: message.city,
        country: message.country,
        date: message.date,
        email: message.email,
        id: message.id,
        ip: message.ip,
        message: message.message,
        name: message.name,
        region: message.region,
        read: message.read,
        favorite: true,
        trash: message.trash,
        trashDate: message.trashDate
      });
    } else {
      this.messageCol.doc(message.email + '. ID: ' + message.id).set({
        city: message.city,
        country: message.country,
        date: message.date,
        email: message.email,
        id: message.id,
        ip: message.ip,
        message: message.message,
        name: message.name,
        region: message.region,
        read: message.read,
        favorite: false,
        trash: message.trash,
        trashDate: message.trashDate
      });
    }
  }
  moveToTrash(message) {
    this.messageCol.doc(message.email + '. ID: ' + message.id).set({
      city: message.city,
      country: message.country,
      date: message.date,
      email: message.email,
      id: message.id,
      ip: message.ip,
      message: message.message,
      name: message.name,
      region: message.region,
      read: message.read,
      favorite: true,
      trash: true,
      trashDate: new Date()
    });
  }
  removeFromTrash(message) {
    this.messageCol.doc(message.email + '. ID: ' + message.id).set({
      city: message.city,
      country: message.country,
      date: message.date,
      email: message.email,
      id: message.id,
      ip: message.ip,
      message: message.message,
      name: message.name,
      region: message.region,
      read: message.read,
      favorite: true,
      trash: false,
      trashDate: null
    });
  }

  messageCount(option, value) {
    if (option === 'favorite') {
      return value.filter(item => item.favorite === true && item.trash === false).length;
    } else if (option === 'trash') {
      return value.filter(item => item.trash === true).length;
    } else {
      return value.filter(item => item.trash === false).length;
    }
  }

}
