import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  scroll(id) {
    const el = document.getElementById(id);
    console.log(el);
    el.scrollIntoView({ behavior: 'smooth'});
  }

}
