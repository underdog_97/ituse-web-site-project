import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    try {
      let headerHeight = document.getElementById('heightBox').offsetHeight;
      document.getElementById("toolbar").style.height = `${headerHeight}px`;
    }
    catch (e) {

    }
  }
}
