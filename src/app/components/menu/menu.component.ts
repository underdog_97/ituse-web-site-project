import { Component, OnInit, EventEmitter, Input, Output, ViewChild, HostListener } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { LoginComponent } from '../login/login.component';
import {MatMenuTrigger} from '@angular/material';

import {
  trigger,
  state,
  style,
  animate,
  transition,
  keyframes
} from '@angular/animations';

import { AuthService } from '../../services/auth.service';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
  animations: [
    trigger('openClose', [
      // ...
      state('open', style({
        'transform': 'translateY(0px)'
      })),
      state('closed', style({

      })),
      transition('closed => open', [
        animate('0.3s', 
        keyframes([
          style({
            'transform': 'translateY(-300px)'
          }),
          style({
            'transform': 'translateY(-100px)'
          }),
          style({
            'transform': 'translateY(0px)'
          }),
        ]))
      ]),
      transition('open => closed', [
        animate('0.5s')
      ]),
    ]),
  ],
})
export class MenuComponent implements OnInit {
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  public href: string = "";
  state: string = 'closed';
  isLogin: String;
  offsetY = 0;
  isOpen = false;
  constructor(public dialog: MatDialog, 
    public authService: AuthService) {}

  ngOnInit() {

  }
  dialogRef: any;
  scroll(id) {
    const yourElement = document.getElementById(id);

    this.trigger.closeMenu();

    const yCoordinate = yourElement.getBoundingClientRect().top + window.pageYOffset;
    let yOffset = -73;



    if(window.innerWidth < 1150) {
      yOffset = -40;
    }

    else {
      if (id === 'intouchContainer') {
        yOffset = 0;
      }
    }


    window.scrollTo({
        top: yCoordinate + yOffset,
        behavior: 'smooth'
    });

  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll($event) {
    this.state = window.pageYOffset > 75 ? 'open' : 'closed';
    
    if (window.pageYOffset > 75) {
      let element = document.getElementById('navbar');
      element.classList.add('sticky');
    } else {
      let element = document.getElementById('navbar');
      element.classList.remove('sticky'); 
    }
  }

  checkOffsetY() {
    let val = 100;
    if (window.pageYOffset > val) {
      return true;
    }
    else {
      return false;
    }
  }

  openPopup() {
    this.dialogRef = this.dialog.open(LoginComponent, {
      width: '350px',
    });
  }
}
