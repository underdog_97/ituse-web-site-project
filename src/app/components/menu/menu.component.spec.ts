import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImplementMenuComponent } from './implement-menu.component';

describe('ImplementMenuComponent', () => {
  let component: ImplementMenuComponent;
  let fixture: ComponentFixture<ImplementMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImplementMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImplementMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
