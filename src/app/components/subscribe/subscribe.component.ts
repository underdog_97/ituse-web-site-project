import { Component, OnInit } from '@angular/core';
import { SubscriptionsService } from '../../services/subscriptions.service';
import { Subscription } from '../../models/Subscription';
import { FormsModule, FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.css']
})
export class SubscribeComponent implements OnInit {

  subscription: Subscription = {
    email:''
  }
  myForm = new FormGroup({
    email: new FormControl('')
  });
  constructor(private fb: FormBuilder, 
    private subService: SubscriptionsService, 
    private http: HttpClient,
    private _snackBar: MatSnackBar) { }

  ngOnInit() {
    
  }
  sendEmail() {
    if (this.myForm.valid === true) {
      this.subscription.email = this.myForm.value.email;
      this.subService.addSubscription(this.subscription);
      this.sendSnackBar(`${this.myForm.value.email.substr(0, 50)} : successfully added to subscribe list!`, 'accept', 3000);
      this.myForm.value.email = '';
    }
    else {
      if (this.myForm.value.email.length > 50) {
        this.sendSnackBar(`${this.myForm.value.email.substr(0, 40)}... invalid for subscribing!`, "warning", 3000);
      }
      else {
        this.sendSnackBar(`${this.myForm.value.email} invalid for subscribing!`, "warning", 3000);
      }
    }
  }
  sendSnackBar(message, type, time) {
    if (type == 'warning') {
      this._snackBar.open(message, 'OK', {
        duration: time,
        verticalPosition: 'bottom',
        horizontalPosition: 'center',
        panelClass: ['warning']
      });
    }
    else if (type == 'accept') {
      this._snackBar.open(message, 'OK', {
        duration: time,
        verticalPosition: 'bottom',
        horizontalPosition: 'center',
        panelClass: ['accept-snackbar']
      });
    }
  }

}
