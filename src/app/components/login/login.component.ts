import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';  
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email: String;
  password: String;
  constructor(public dialog: MatDialog, public authService: AuthService) { }
  
  ngOnInit() {
  }

  closeDialog(){
    this.dialog.closeAll();
  }
}
