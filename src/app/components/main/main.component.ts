import { Component, OnInit, HostListener } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { TgBotService } from '../../services/tg-bot.service';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  ipClient = ''
  offsetY = 0;

  constructor(private titleService: Title,
              private bot: TgBotService) {
    this.titleService.setTitle("ITUse");
  }

  @HostListener('window:scroll', ['$event'])
    onWindowScroll($event) {
      this.offsetY = window.pageYOffset;
    }

  scrollTo(y) {
    window.scrollTo({
      behavior: 'smooth',
      top: 0
    })
  }

  ngOnInit() {
    this.bot.checkIp();
  }

}
