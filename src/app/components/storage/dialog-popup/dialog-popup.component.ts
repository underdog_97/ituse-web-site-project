import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ViewChild, ElementRef } from '@angular/core';
import { Image } from 'src/app/models/Image';
import { StorageService } from 'src/app/services/storage.service';

export interface DialogData {
  item: Image,
  deleteFunction: boolean
}

@Component({
  selector: 'app-dialog-popup',
  templateUrl: './dialog-popup.component.html',
  styleUrls: ['./dialog-popup.component.css']
})



export class DialogPopupComponent implements OnInit {

  currentMode: string = "none";
  
  
  constructor(public dialogRef: MatDialogRef<DialogPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public ss: StorageService) { }

  ngOnInit() {
  }
  
  delete(item) {
    this.ss.deleteImage(item);
    this.onNoClick()
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  openNewWindow(url) {
    window.open(url);
  }
}