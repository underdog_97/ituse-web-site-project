import { Component, OnInit, Inject } from '@angular/core';
import { StorageService } from '../../services/storage.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ViewChild, ElementRef } from '@angular/core';
import { Image } from '../../models/Image';
import { DialogPopupComponent } from './dialog-popup/dialog-popup.component';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

export interface Folder {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-storage',
  templateUrl: './storage.component.html',
  styleUrls: ['./storage.component.css']
})
export class StorageComponent implements OnInit {

  @ViewChild('fileInput')
  myInputVariable: ElementRef;
  currentMode = 'gallery';
  sortBy = 'all';
  hover = false;
  storage: Array<Image>;
  uploadEvent: any;
  selectedFolder = '';
  storageName =  '';
  downloadUrl = '';

  folders: Folder[] = [
    {value: 'all', viewValue: 'All'},
    {value: 'team', viewValue: 'Team'},
    {value: 'portfolio', viewValue: 'Portfolio'},
    {value: 'other', viewValue: 'Other'}
  ];

  constructor(private ss: StorageService,
              public dialog: MatDialog,
              private router: Router,
              private titleService: Title) { }

  ngOnInit() {
    this.updateItems();
    this.titleService.setTitle("Storage");
  }

  openDialog(item) {
    const dialogRef = this.dialog.open(DialogPopupComponent, {
      minWidth: '10px',
      data: {item,
        deleteFunction: true}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.updateItems();
    });
  }

  updateItems() {
    this.ss.getItems().subscribe(
      result => {
        this.storage = result;
      }
    );
  }

  resetFileUpload() {
    this.myInputVariable.nativeElement.value = '';
    this.selectedFolder = '';
    this.storageName = '';
    this.uploadEvent = '';
}

  uploadFile() {
    this.ss.addImage(this.uploadEvent, this.storageName, this.selectedFolder);
    this.sortBy = this.selectedFolder;
    this.resetFileUpload();
    this.currentMode = 'gallery';
  }

  inputChange(event) {
    this.uploadEvent = event;
  }

  checkValid() {
    // tslint:disable-next-line: triple-equals
    if (this.selectedFolder.length > 0 && this.storageName.length > 0 && this.uploadEvent != undefined) {
      return false;
    } else {
      return true;
    }
  }
  getImageUrl(item) {
    return 'url(\'' + item.url + '\')';
  }
  gridClasses(i) {
    const tempIndex = i % 4 + 1;
    return 'square' + tempIndex;
  }
  navigate(url) {
    window.open(url);
  }
}
