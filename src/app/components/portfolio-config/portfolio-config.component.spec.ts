import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortfolioConfigComponent } from './portfolio-config.component';

describe('PortfolioConfigComponent', () => {
  let component: PortfolioConfigComponent;
  let fixture: ComponentFixture<PortfolioConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortfolioConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortfolioConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
