import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Image } from '../../models/Image';
import { Portfolio_Cat } from '../../models/Portfolio_Cat';
import { PortfolioService } from '../../services/portfolio.service';
import { StorageService } from '../../services/storage.service';
import { DialogPopupComponent } from '../storage/dialog-popup/dialog-popup.component';

type PortfolioMode = 'changePos' | 'rename' | 'none' | 'newImage';

@Component({
  selector: 'portfolio-config',
  templateUrl: './portfolio-config.component.html',
  styleUrls: ['./portfolio-config.component.css']
})
export class PortfolioConfigComponent implements OnInit {
  storage: Array<Image>;
  addNewVisible = true;
  currentIndex: number;
  currentPage = 'cats';
  galleryChangeMode = false;
  currentMode: PortfolioMode = 'none';
  portfolioCats: Array<Portfolio_Cat> = [];
  portfolio: Portfolio_Cat = {
    name: '',
    position: null,
    id: '',
    images: []
  };

  constructor(public ps: PortfolioService,
              public dialog: MatDialog,
              public ss: StorageService) { }

  ngOnInit(): void {
    this.updateCats();
    this.updateItems();
    this.currentIndex = 0;
  }
  setValue(value): void {
        this.galleryChangeMode = !value;
}
  updateItems(): void {
    this.ss.getItems()
    .subscribe(
      result => {
        this.storage = result.filter(value => value.folder === 'portfolio');
      }
    );
  }
  openDialog(item): void {
    const dialogRef = this.dialog.open(DialogPopupComponent, {
      minWidth: '10px',
      data: {item,
             deleteFunction: false}
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        this.updateItems();
      });
  }
  addCat(): void {
    if (this.portfolio.name === '') {
      return;
    }
    this.portfolio.position = this.portfolioCats.length;
    this.ps.addCategory(this.portfolio);
    this.portfolio.name = '';
    this.portfolio.position = null;
    this.addNewVisible = !this.addNewVisible;
  }

  catChange(portfolioCat: Portfolio_Cat): void {
    this.currentIndex = this.portfolioCats.indexOf(portfolioCat);
  }

  deleteCat(portfolioCat: Portfolio_Cat): void {
    this.currentIndex = 0;
    this.ps.deleteCategory(portfolioCat);
    this.updateCats();
  }

  saveCat(): void {
    this.ps.renameCat(this.portfolioCats[this.currentIndex]);
    this.currentMode = 'none';
    this.updateCats();
  }

  saveList(portfolioCat: Array<Portfolio_Cat>): void {
    for (let i = 0; i < portfolioCat.length; i++) {
      this.ps.renameCat(this.portfolioCats[i]);
    }
    this.currentMode = 'none';
    this.updateCats();
  }

  getImageUrl(item): string {
    return `url('${item.url}')`;
  }

  cancelMode(): void {
    this.currentMode = 'none';
  }

  updateCats(): void {
    this.ps.getCategories()
      .subscribe(value => {
        this.portfolioCats = value;
        this.portfolioCats.sort(
          (a, b) => a.position - b.position);
      });
  }

  goLeft(): void {
    const currentElem = this.portfolioCats[this.currentIndex];
    currentElem.position = this.currentIndex - 1;
    const prevElem = this.portfolioCats[this.currentIndex - 1];
    prevElem.position = this.currentIndex;
    this.portfolioCats.splice(this.currentIndex - 1, 2, currentElem, prevElem);
    this.currentIndex = this.currentIndex - 1;
  }

  goRight(): void {
    const currentElem = this.portfolioCats[this.currentIndex];
    currentElem.position = this.currentIndex + 1;
    const nextElem = this.portfolioCats[this.currentIndex + 1];
    nextElem.position = this.currentIndex;
    this.portfolioCats.splice(this.currentIndex, 2, nextElem, currentElem);
    this.currentIndex = this.currentIndex + 1;
  }

  choose(item): void {
    if (this.portfolioCats[this.currentIndex].images.includes(item.url)) {
      const itemIndexInArray = this.portfolioCats[this.currentIndex].images.indexOf(item.url);
      this.portfolioCats[this.currentIndex].images.splice(itemIndexInArray, 1);
    } else {
      this.portfolioCats[this.currentIndex].images.push(item.url);
    }
    this.ps.renameCat(this.portfolioCats[this.currentIndex]);
  }

  toggleAddNewVisible(): void {
    this.addNewVisible = !this.addNewVisible;
  }

  setMode(newMode: PortfolioMode): void {
    this.currentMode = newMode;
  }
}
