import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamConfigDialogComponent } from './team-config-dialog.component';

describe('TeamConfigDialogComponent', () => {
  let component: TeamConfigDialogComponent;
  let fixture: ComponentFixture<TeamConfigDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamConfigDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamConfigDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
