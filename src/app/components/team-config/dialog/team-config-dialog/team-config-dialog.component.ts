import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TeamConfigService } from '../../../../services/team-config.service';
import { Worker } from '../../../../models/Worker';

@Component({
  selector: 'app-team-config-dialog',
  templateUrl: './team-config-dialog.component.html',
  styleUrls: ['./team-config-dialog.component.css']
})
export class TeamConfigDialogComponent implements OnInit {

  constructor(private tcs: TeamConfigService,
    public dialog: MatDialog, 
    @Inject(MAT_DIALOG_DATA) public data: Worker) { }

  ngOnInit() {
  }
  closeDialog(){
    this.dialog.closeAll();
  }
  deleteMate(data) {
    this.tcs.deleteWorker(data);
    this.dialog.closeAll();
  }
}
