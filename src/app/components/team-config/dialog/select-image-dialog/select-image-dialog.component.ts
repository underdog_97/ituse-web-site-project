import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';
import { Image } from '../../../../models/Image';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
@Component({
  selector: 'app-select-image-dialog',
  templateUrl: './select-image-dialog.component.html',
  styleUrls: ['./select-image-dialog.component.css']
})
export class SelectImageDialogComponent implements OnInit {

  storage: Array<Image>;

  constructor(private ss: StorageService,
    public dialogRef: MatDialogRef<SelectImageDialogComponent>) { }

  ngOnInit() {
    this.ss.getItems().subscribe(
      result => {
        this.storage = result.filter(result => result.folder == 'team');
      }
    )
  }
  closeDialog(url) {
    this.dialogRef.close(url);
  }

}
