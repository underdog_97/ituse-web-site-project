import { Component, OnInit } from '@angular/core';
import { TeamConfigService } from '../../services/team-config.service';
import { Worker } from '../../models/Worker';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { FormsModule, FormGroup, FormBuilder, FormControl, Validator, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TeamConfigDialogComponent } from './dialog/team-config-dialog/team-config-dialog.component';
import { SelectImageDialogComponent } from './dialog/select-image-dialog/select-image-dialog.component';
@Component({
  selector: 'app-team-config',
  templateUrl: './team-config.component.html',
  styleUrls: ['./team-config.component.css']
})
export class TeamConfigComponent implements OnInit {
  changeModeVar: Boolean = false;
  currentMate: number = 0;
  dialogRef: any;
  teamCol: AngularFirestoreCollection<Worker>;
  team: Observable<Worker[]>;
  teamList: Array<Worker>;
  currentPage: string = 'teamList';
  addProfile: FormGroup;
  addedWorker: Worker = {
    name: '',
    description: '',
    photo_url: '',
    socials: [],
    id: '',
    position: null
  }
  checkImgTouch: boolean = false;
  constructor(public tcs: TeamConfigService, 
    private afs: AngularFirestore, 
    public dialog: MatDialog) { }
  ngOnInit() {
    this.teamCol = this.afs.collection('team');
    this.team = this.teamCol.valueChanges();
    this.team.subscribe(value => {
      this.teamList = value;
      this.teamList.sort(function(a, b) {
        return a.position - b.position;
    });
    })
      this.addProfile = new FormGroup({
      name: new FormControl(''),
      description: new FormControl(''),
      image: new FormControl(''),
      socialName1: new FormControl(''),
      socialLink1: new FormControl(''),
      socialName2: new FormControl(''),
      socialLink2: new FormControl(''),
      socialName3: new FormControl(''),
      socialLink3: new FormControl(''),
    })
  }

  addNewToggle(currentPage) {
    switch(currentPage) {
      case 'teamList':  // if (x === 'value1')
        this.currentPage = 'teamList';
        break;
      case 'addNew':  // if (x === 'value2')
        this.currentPage = 'addNew';
        break;
      case 'changePosition':
        this.currentPage = 'changePosition';
        break;
    }
  }

  submit() {
    this.convertFromFormToClass();
    this.tcs.addWorker(this.addedWorker);
    this.currentPage = 'teamList';
    this.addProfile.reset();
    this.addedWorker = {
      name: '',
      description: '',
      photo_url: '',
      socials: [],
      id: '',
      position: null
    }
  }

  changeMode() {
    this.changeModeVar = !this.changeModeVar;
  }

  convertFromFormToClass () {
    this.addedWorker.name = this.addProfile.value.name;
    this.addedWorker.description = this.addProfile.value.description;
    this.addedWorker.photo_url = this.addProfile.value.image;
    this.addedWorker.socials.push({social_name: this.addProfile.value.socialName1,
      profile_link: this.addProfile.value.socialLink1},
      {social_name: this.addProfile.value.socialName2,
        profile_link: this.addProfile.value.socialLink2},
        {social_name: this.addProfile.value.socialName3,
          profile_link: this.addProfile.value.socialLink3});
    this.addedWorker.position = this.teamList.length;
  }

  goLeft() {
    this.currentMate -= 1;
    this.changeModeVar = false;
  }

  goRight() {
    this.currentMate += 1;
    this.changeModeVar = false;
  }

  changePos(index) {
    this.currentMate = index;
  }

  openPopup(worker: Worker) {
    this.currentMate = 0;
    this.dialogRef = this.dialog.open(TeamConfigDialogComponent, {
      height: '330px',
      width: '370px',
      data: worker,
    });

  }

  openPopupImage(worker: Worker) {
    this.dialogRef = this.dialog.open(SelectImageDialogComponent, {
      width: '600px',
      data: worker,
    });
    this.dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.teamList[this.currentMate].photo_url = result;
      }
    });
  }
  openPopupNewImage(worker: Worker) {
    this.dialogRef = this.dialog.open(SelectImageDialogComponent, {
      width: '600px',
      data: worker,
    });
    this.dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.addProfile.get('image').setValue(result);
      }
    });
  }

  switchLeft(currentMate) {
    var currentElem = this.teamList[currentMate];
    currentElem.position = currentMate - 1;
    var prevElem = this.teamList[currentMate - 1];
    prevElem.position = currentMate;
    this.teamList.splice(currentMate-1, 2, currentElem, prevElem);
    this.currentMate = this.currentMate - 1;
    this.checkImgTouch = true;
  }

  switchRight(currentMate) {
    var currentElem = this.teamList[currentMate];
    currentElem.position = currentMate + 1;
    var nextElem = this.teamList[currentMate+1];
    nextElem.position = currentMate;
    this.teamList.splice(currentMate, 2, nextElem, currentElem);
    this.currentMate = this.currentMate +1;
    this.checkImgTouch = true;
  }
  saveList() {
    this.checkImgTouch = false;
    this.tcs.saveTeamList(this.teamList);
    this.team.subscribe(value => {
      this.teamList = value;
      this.teamList.sort(function(a, b) {
        return a.position - b.position;
    });
    })
  }
  changes(option) {
    if (option == 'save') {
      this.changeMode();
      this.tcs.saveTeammate(this.teamList[this.currentMate]);
      this.team.subscribe(value => {
        this.teamList = value;
        this.teamList.sort(function(a, b) {
          return a.position - b.position;
        });
      });
    }
    else if (option == 'cancel') {
      console.log(option)
      this.changeMode();
      this.team.subscribe(value => {
        this.teamList = value;
        this.teamList.sort(function(a, b) {
          return a.position - b.position;
        });
      });
    }
    this.team = this.teamCol.valueChanges();
  }
}