import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { Worker } from '../../models/Worker';
import { TeamConfigService } from '../../services/team-config.service';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit {
  teamCol: AngularFirestoreCollection<Worker>;
  team: Observable<Array<Worker>>;
  teamList: Array<Worker> = [];
  currentMate = 0;
  isOpen = false;
  constructor(public tcs: TeamConfigService,
              private readonly afs: AngularFirestore) { }

  ngOnInit() {
    this.teamCol = this.afs.collection('team');
    this.team = this.teamCol.valueChanges();
    this.team.subscribe(value => {
      this.teamList = value;
      this.teamList.sort(function(a, b) {
      return a.position - b.position;
  });
    });

  }

  goLeft() {
    this.currentMate = this.currentMate - 1;

  }
  goRight() {
    this.currentMate = this.currentMate + 1;
  }

  changePos(mate) {
    for (let i = 0; i < this.teamList.length; i++) {
      if (this.teamList[i].name === mate.name) {
        this.currentMate = i;
      }
    }
  }

}
