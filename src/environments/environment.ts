// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCfLzg9F4VP_ctnPwGJ0XYKup6v3tJl8c0',
    authDomain: 'ituse-dev.firebaseapp.com',
    databaseURL: 'https://ituse-dev.firebaseio.com',
    projectId: 'ituse-dev',
    storageBucket: 'ituse-dev.appspot.com',
    messagingSenderId: '712085458467',
    appId: '1:712085458467:web:3948c5be5609c78f68eb96',
    measurementId: 'G-SKE9VSNLTK'
}
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
